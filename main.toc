\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Broad Question}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Purpose}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Testable Question}{3}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Scope}{3}{subsection.1.4}%
\contentsline {section}{\numberline {2}Background}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Earlier Research}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Terms and Definitions}{4}{subsection.2.2}%
\contentsline {subsubsection}{\numberline {2.2.1}Neural Network Terminology and explanation}{4}{subsubsection.2.2.1}%
\contentsline {subsubsection}{\numberline {2.2.2}Programming Terminology}{6}{subsubsection.2.2.2}%
\contentsline {section}{\numberline {3}Method}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Data Collection}{6}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Source}{6}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}Data Window}{6}{subsubsection.3.1.2}%
\contentsline {subsubsection}{\numberline {3.1.3}Data Cleaning}{6}{subsubsection.3.1.3}%
\contentsline {subsection}{\numberline {3.2}Framework Development}{6}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Data structures}{6}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Api usability concerns}{8}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Activation function usage and its implications}{9}{subsubsection.3.2.3}%
\contentsline {subsubsection}{\numberline {3.2.4}Cost function implementation}{10}{subsubsection.3.2.4}%
\contentsline {subsubsection}{\numberline {3.2.5}Epoch execution process}{12}{subsubsection.3.2.5}%
\contentsline {subsection}{\numberline {3.3}Model Structure}{13}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Activation function selection}{13}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Cost function selection}{14}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}Network size}{15}{subsubsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.4}Weight and bias initialization}{16}{subsubsection.3.3.4}%
\contentsline {section}{\numberline {4}Result}{16}{section.4}%
\contentsline {section}{\numberline {5}Discussion}{16}{section.5}%
\contentsline {section}{\numberline {6}Conclusion}{16}{section.6}%
\contentsline {section}{\numberline {7}References}{16}{section.7}%
\contentsline {section}{\numberline {8}Supplements}{16}{section.8}%
